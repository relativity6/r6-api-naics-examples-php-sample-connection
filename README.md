# EXAMPLE - HTTP REQUEST TO API NAICS IDENTIFIER

## Getting Started

Following these instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Php you needs to run the software:

|   Sofware    |   Version   |
| :---------:  | :---------: |
|   php        | PHP 8.0.10  |

## Usage

In order to run the process, make sure that you have the proper token to send requests. Once you have the token, follow the next steps to execute the code:

1. Replace '**YOUR_TOKEN_HERE**' with your **token**
2. Run the code

**ATTENTION**: It is necessary to run the code with an IDE to view the results in console.

## Built With

* [Sublime Text](https://www.sublimetext.com) - cross-platform code editor
* [PHP](https://www.php.net) - Programming language
* [Github](https://github.com/) - Code Hosting Platform for Version Control

## Authors

* **Ayala Alatorre Alexis** - *Developer* - alexis@relativity6.com

## License

This project is private, owned by "Relativity6"
