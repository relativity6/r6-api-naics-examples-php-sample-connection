<?php 

$sUrl = "https://api.usemarvin.ai/naics/naics/search/";

$array = array(
	'token'=> 'YOUR_TOKEN_HERE',
   	'name'=> 'Apple Inc',
   	'address'=> 'One Apple Park Way',
   	'state'=> 'CA'
);


$myJson = json_encode($array);

$data = http_build_query($array);

$ch = curl_init();

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
));
curl_setopt($ch, CURLOPT_URL, $sUrl);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $myJson);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$resp = curl_exec($ch);

if (curl_error($ch)) 
{
	echo curl_error($ch);
}
else
{

	$decoded = json_decode($resp, true);
	$jsonResponse = json_encode(json_decode($resp),JSON_PRETTY_PRINT);

	echo '<pre>'.$jsonResponse.'</pre>';

} 
curl_close($ch);

?>